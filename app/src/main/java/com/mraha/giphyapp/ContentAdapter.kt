package com.mraha.giphyapp

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.mraha.giphyapp.model.GIF
import java.io.File
import java.io.FileOutputStream
import java.nio.ByteBuffer


class ContentAdapter(private val context: Context, private val data: ArrayList<GIF>) :
    RecyclerView.Adapter<ContentAdapter.MViewHolder>() {

    class MViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val gifImageView: ImageView = view.findViewById(R.id.gifImageView)
        val progressBar: ProgressBar = view.findViewById(R.id.progressBar)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.gif_item_view, parent, false)
        return MViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.progressBar.visibility = View.VISIBLE

        Glide.with(context).asGif()
            .load(data[position].images.downsized.url)
            .listener(object : RequestListener<GifDrawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progressBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    val uri = saveImageAndGetURI(resource, position)
                    holder.gifImageView.setOnClickListener {
                        val intent = Intent(Intent.ACTION_SEND);
                        intent.type = "image/gif";
                        intent.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(Intent.createChooser(intent, "Share image using"))
                    }
                    holder.progressBar.visibility = View.GONE
                    return false
                }
            }).into(holder.gifImageView)
    }


    private fun saveImageAndGetURI(gifDrawable: GifDrawable?, position: Int): Uri {
        val baseDir = context.cacheDir.absolutePath
        val fileName = "sharingGif$position.gif"
        val sharingGifFile = File(baseDir, fileName)
        if (gifDrawable != null) {
            gifDrawableToFile(gifDrawable, sharingGifFile)
        }
        return FileProvider.getUriForFile(
            context,
            "com.mraha.giphyapp.fileprovider",
            sharingGifFile
        )
    }

    private fun gifDrawableToFile(gifDrawable: GifDrawable, gifFile: File) {
        val byteBuffer = gifDrawable.buffer
        val output = FileOutputStream(gifFile)
        val bytes = ByteArray(byteBuffer.capacity())
        (byteBuffer.duplicate().clear() as ByteBuffer).get(bytes)
        output.write(bytes, 0, bytes.size)
        output.close()
    }
}