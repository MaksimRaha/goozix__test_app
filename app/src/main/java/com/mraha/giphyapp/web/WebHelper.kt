package com.mraha.giphyapp.web

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WebHelper {
    fun createWebService(): WebService {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.giphy.com/v1/stickers/").addConverterFactory(
                GsonConverterFactory.create()).build()
        return retrofit.create(WebService::class.java)
    }
}