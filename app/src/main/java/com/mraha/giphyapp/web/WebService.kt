package com.mraha.giphyapp.web

import com.mraha.giphyapp.model.Data
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.http.Query

interface WebService {
    companion object {
        const val API_KEY = "api_key=jtcwpzELTUJIq6s0uZXv0tQ6PIyd55W4"
    }

    @GET("trending?$API_KEY")
    fun getTrendingData(): Call<Data>

    @GET("search?$API_KEY")
    fun searchGifWithQueryWord(@Query("q") word: String): Call<Data>

}