package com.mraha.giphyapp.web

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.mraha.giphyapp.ContentAdapter
import com.mraha.giphyapp.model.Data
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WebCallback(private val context: Context, private val recyclerView: RecyclerView) : Callback<Data> {

    override fun onFailure(call: Call<Data>, t: Throwable) {
    }

    override fun onResponse(call: Call<Data>, response: Response<Data>) {
        val body = response.body()
        if (body != null) {
            recyclerView.adapter = ContentAdapter(context, body.data)
            (recyclerView.adapter as ContentAdapter).notifyDataSetChanged()
        }
    }
}