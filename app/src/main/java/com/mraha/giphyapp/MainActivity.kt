package com.mraha.giphyapp

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.mraha.giphyapp.web.WebCallback
import com.mraha.giphyapp.web.WebHelper
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val recyclerView = lazy { mRecyclerView }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        WebHelper().createWebService().getTrendingData().enqueue(WebCallback(this, recyclerView.value))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.search_item -> {
                showSearchDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showSearchDialog() {
        val editText = EditText(this)
        AlertDialog.Builder(this).setView(editText)
            .setTitle("Search...")
            .setNegativeButton("cancel") { dialogInterface, i -> dialogInterface.dismiss() }
            .setPositiveButton("ok") { dialogInterface: DialogInterface, i: Int ->
                WebHelper().createWebService().searchGifWithQueryWord(editText.text.toString())
                    .enqueue(WebCallback(this, recyclerView.value))
            }.show()
    }
}